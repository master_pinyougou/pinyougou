package com.pinyougou.sellergoods.service.impl;

import com.pinyougou.mapper.BrandMapper;
import com.pinyougou.pojo.Brand;
import com.pinyougou.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.transaction.annotation.Transactional;

import java.net.InterfaceAddress;
import java.util.List;
@Service(interfaceName="com.pinyougou.service.BrandService")
//上面指定接口名，不然会采用代理类名称
@Transactional
public class BrandServiceImpl implements BrandService {
	//注入数据访问接口代理对象
	@Autowired
	private BrandMapper brandMapper;

	@Override
	public List<Brand> findAll() {

		return brandMapper.findAll();
	}
}
