package com.pinyougou.mapper;

import com.pinyougou.pojo.Brand;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface BrandMapper {
	@Select("SELECT * FROM tb_brand order BY id ASC ")
	List<Brand> findAll();
}
